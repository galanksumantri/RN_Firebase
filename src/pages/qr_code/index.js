import { StyleSheet, View, Alert, Text } from 'react-native';
import React from 'react';
import { CameraScreen } from 'react-native-camera-kit';

export default function QrCode() {
  function onBottomButtonPressed(event) {
    const captureImages = JSON.stringify(event.captureImages);
    Alert.alert(
      `"${event.type}" Button Pressed`,
      `${captureImages}`,
      [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      { cancelable: false },
    );
  }

  return (
    <View style={{ height: 740 }}>
      <CameraScreen
        actions={{ rightButtonText: 'Done', leftButtonText: 'Qr scan' }}
        onBottomButtonPressed={(event) => onBottomButtonPressed(event)}
        style={{ flex: 1 }}
        flashImages={{
          on: require('../../assets/images/flashOn.png'),
          off: require('../../assets/images/flashOff.png'),
          auto: require('../../assets/images/flashAuto.png'),
        }}
        cameraFlipImage={require('../../assets/images/cameraFlipIcon.png')} 
        captureButtonImage={require('../../assets/images/cameraButton.png')} 
        torchOnImage={require('../../assets/images/torchOn.png')} 
        torchOffImage={require('../../assets/images/torchOff.png')} 
        hideControls={false} 
        showCapturedImageCount={false} 
        scanBarcode
        onReadCode={(event) => Alert.alert(`url : ${event.nativeEvent.codeStringValue}`)} 
        showFrame 
        laserColor="red" 
        frameColor="white" 
      />
      <Text style={{ marginBottom: 30 }}>Ini QrCode Screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  inputForm: {
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 10,
  },
  container: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  button: {
    backgroundColor: 'blue',
    marginTop: 20,
    padding: 10,
    borderRadius: 8,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
  },
});
