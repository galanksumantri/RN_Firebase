import {
  StyleSheet, Text, TextInput, TouchableOpacity, View, Button, TouchableHighlight
} from 'react-native';
import React, { useState } from 'react';
import auth from '@react-native-firebase/auth';
import analytics from '@react-native-firebase/analytics';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import * as navigation from '../../config/RootNavigation';
import TouchID from 'react-native-touch-id';

export default function Login() {
  GoogleSignin.configure({
    webClientId:
    '205908597046-e51cip8lh5cnm3kvveme3srn9i78bluq.apps.googleusercontent.com',
  });
  const [email, setemail] = useState('');
  const [password, setpassword] = useState('');
  const onChangeEmail = (input) => {
    setemail(input);
  };
  const onChangePassword = (input) => {
    setpassword(input);
  };

  async function onGoogleButtonPress() {
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }

  // const FingerPrint = () => {
  const OptionalConfigObject = {
    tittle: "Authentication Required",
    color: "#e00606",
    fallbackLabel: "Show Passcode"
  }

  const pressHandler = () => {
    TouchID.authenticate("to demo this react-native component", OptionalConfigObject)
      .then(succes => {
        alert(succes)
      })
      .catch(error => {
        alert("Authentication Failed")
      })
  }

  return (
    <View style={styles.container}>
      <Text style={styles.tittleText}>MY APP</Text>
      <TextInput
        placeholder="E-mail"
        value={email}
        onChangeText={onChangeEmail}
        style={styles.inputForm}
      />
      <TextInput
        placeholder="Password"
        secureTextEntry
        value={password}
        onChangeText={onChangePassword}
        style={styles.inputForm}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Main')}
      >
        <Text style={styles.buttonText}>LOGIN</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={() => onGoogleButtonPress().then(() => navigation.navigate('Main'))}>
        <Text style={styles.buttonText}>LOGIN WITH GOOGLE</Text>
      </TouchableOpacity>

      <TouchableOpacity 
        style={styles.button} 
        onPress={async () => await analytics().logEvent('basket', {
          id: 3745092,
          item: 'mens grey t-shirt',
          description: ['round neck', 'long sleeved'],
          size: 'L',
        })}>
        <Text style={styles.buttonText}>ADD TO BASKET</Text>
      </TouchableOpacity>

      <TouchableHighlight style={styles.button} onPress={() => pressHandler()}>
        <Text style={styles.buttonText}>
          Authenticate With Touch ID
        </Text>
      </TouchableHighlight>
      <TouchableHighlight style={styles.button} onPress={() => clickHandler()}>
        <Text style={styles.buttonText}>
          Check is supported for biometryType
        </Text>
      </TouchableHighlight>
    </View>
  );
}

const styles = StyleSheet.create({
  inputForm: {
    borderWidth: 1,
    marginTop: 10,
    borderRadius: 10,
  },
  container: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  button: {
    backgroundColor: 'blue',
    marginTop: 20,
    padding: 10,
    borderRadius: 8,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
  },
  tittleText: {
    color: 'black',
    textAlign: 'center',
    fontSize: 20,
  },
});
