import React, { useState, useCallback } from 'react';
import { RefreshControl, ScrollView, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import VectorImage from 'react-native-vector-image';
import { Home, QrCode, Login, App, Apps } from '../../pages';
import { navigationRef } from '../RootNavigation';

const wait = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));

function HomeScreen() {
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => setRefreshing(false));
  }, []);
  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
    }
    >
      <Home />
    </ScrollView>
  );
}
function QrScreen({ route }) {
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => setRefreshing(false));
  }, []);
  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
    }
    >
      <QrCode />
    </ScrollView>
  );
}

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function MainApp() {
  return (
    <Tab.Navigator screenOptions={{ headerShown: false, tabBarShowLabel: false }}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <VectorImage source={require('../../assets/icons/fi_home.svg')} color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="QR Code"
        component={QrScreen}
        options={{
          tabBarLabel: 'QR Code',
          tabBarIcon: ({ color, size }) => (
            <VectorImage source={require('../../assets/icons/u_capture.svg')} color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

function Routing() {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Apps"
          component={Apps}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="App"
          component={App}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Main"
          component={MainApp}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routing;